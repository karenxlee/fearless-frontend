function createCard(title, description, pictureUrl, dateRange, location) {
    return `
        <div class= "shadow p-3 mb-5 bg-body-tertiary rounded">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top" alt="Conference Image">
                <div class="card-body">
                    <h5 class="card-title">${title}</h5>
                    <p class="card-text">${description}</p>
                </div>
                <ul class="list-group list-group-flush">
                <li class="list-group-item">${location}</li>
                </ul>
                <div class="card-footer">${dateRange}</div>

            </div>
        </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.error('bad response error occured.')

      } else {
        const data = await response.json();
        const columns = document.querySelectorAll('.col');
        console.log(columns);
        let columnIndex = 0;

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const startDate = new Date(details.conference.starts).toLocaleDateString('en-US', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
              });
            const endDate = new Date(details.conference.ends).toLocaleDateString('en-US', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
              });
            const dateRange = `${startDate} - ${endDate}`;
            const html = createCard(title, description, pictureUrl, dateRange, location);
            const column = columns[columnIndex];
            console.log(column);
            column.innerHTML += html;
            columnIndex = (columnIndex + 1) % columns.length

          }
        }

      }
    } catch (e) {
        console.error('An error occured.')
        alert('An error occurred');
    }

  });
